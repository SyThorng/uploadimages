package com.example.uploadimges.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Respones<T> {

    String message;
    T payload;
    LocalDateTime localDateTime;
}
