package com.example.uploadimges;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UploadImgesApplication {

    public static void main(String[] args) {
        SpringApplication.run(UploadImgesApplication.class, args);
    }

}
