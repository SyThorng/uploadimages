package com.example.uploadimges.controller;

import com.example.uploadimges.model.Image;
import com.example.uploadimges.model.Respones;
import com.example.uploadimges.service.serviceImp.FileUploadServiceImp;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;

@RestController
public class FileUploadController {

    private final FileUploadServiceImp fileUploadServiceImp;

    public FileUploadController(FileUploadServiceImp fileUploadServiceImp) {
        this.fileUploadServiceImp = fileUploadServiceImp;
    }

    @PostMapping(value = "/update-image",consumes = {"multipart/form-data"})
    public ResponseEntity<Respones<Image>> uploadImageToServer(
            @RequestParam("file")MultipartFile files
            ){

        Respones<Image> respones=Respones.<Image>builder()
                .message("Insert Success..!!")
                .payload(new Image(fileUploadServiceImp.saveFile(files)))
                .localDateTime(LocalDateTime.now()).build();
        return ResponseEntity.ok().body(respones);
    }
}
