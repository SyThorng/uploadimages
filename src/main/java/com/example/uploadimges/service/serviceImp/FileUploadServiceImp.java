package com.example.uploadimges.service.serviceImp;

import com.example.uploadimges.service.FileUploadService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
public class FileUploadServiceImp implements FileUploadService {

    Path path = Paths.get("src/main/resources/images");

    @Override
    public String saveFile(MultipartFile file) {
        String filename = file.getOriginalFilename();
        UUID uuid = UUID.randomUUID();
        filename = uuid + filename;
        Path resolvePath = path;

        if (!filename.isEmpty()) {
            resolvePath = path.resolve(filename);
        }
        try {
            Files.copy(file.getInputStream(), resolvePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            System.out.println("Error Message:{}" + e.getMessage());
        }
        return filename;
    }
}
